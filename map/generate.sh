#!/bin/bash

make all

cp ./images/skeleton.png ./skeleton.png
cp ./images/sands2.png ./sands2.png
cp ./images/orchards.png ./orchards.png

cp ./images/9_0-51576_618-51535_530-564_10076.png ./image1.png
cp ./images/5_2-51727_431-51686_328-3870_6025.png ./image2.png
cp ./images/10_6-51543_144-51501_56-8925_10989.png ./image3.png

cd ..

cp ./examples/new_ashford.png ./map/new_ashford.png
cp ./examples/new_maidstone.png ./map/new_maidstone.png
cp ./examples/new_canterbury.png ./map/new_canterbury.png

cp ./examples/new_ashford.json ./map/new_ashford.json
cp ./examples/new_maidstone.json ./map/new_maidstone.json
cp ./examples/new_canterbury.json ./map/new_canterbury.json

cd apesdk

cd toolkit

./apesdk-json.sh

cd ..

cd ..

mv ./apesdk/toolkit/apesdk-json ./map/apesdk-json

cd map

ls

## examples1

./map2json -f skeleton.png --apesdk

cp ./map.json ./old_skeleton.json
rm ./map2.json


./map2json -f sands2.png --apesdk

cp ./map.json ./old_sands2.json
rm ./map2.json

./map2json -f orchards.png --apesdk

cp ./map.json ./old_orchards.json
rm ./map2.json

## examples2

./map2json -f image1.png --apesdk

cp ./map.json ./old_image1.json
rm ./map2.json

./map2json -f image2.png --apesdk

cp ./map.json ./old_image2.json
rm ./map2.json


./map2json -f image3.png --apesdk

cp ./map.json ./old_image3.json
rm ./map2.json

## cities

./map2json -f new_ashford.png --apesdk

cp ./map.json ./old_ashford.json
rm ./map2.json

./map2json -f new_maidstone.png --apesdk

cp ./map.json ./old_maidstone.json
rm ./map2.json


./map2json -f new_canterbury.png --apesdk

cp ./map.json ./old_canterbury.json
rm ./map2.json

rm *.png

rm *.json

rm ./map2json

rm ./apesdk-json



