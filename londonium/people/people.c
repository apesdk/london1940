/****************************************************************

    people.c

    =============================================================

 Copyright 1996-2023 Tom Barbalet. All rights reserved.

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.

    This software is a continuing work of Tom Barbalet, begun on
    13 June 1996. No apes or cats were harmed in the writing of
    this software.

****************************************************************/

// command line interface

#include "people.h"

#include <stdio.h>
#include <stdlib.h>

static simulated_person people[PEOPLE_NUMBER];

void people_previous(void * ptr, n_console_output output_function)
{
    
}

void people_next(void * ptr, n_console_output output_function)
{
    
}

n_int people_cycle(void * ptr)
{
    return 0;
}

void people_watch_entity( void *ptr, n_console_output output_function )
{
    
}

n_byte4 people_n_byte4_random(n_byte2 *local)
{
    n_byte4 return_value = 0;
    math_random3(local);
    return_value = math_random(local);
    math_random3(local);
    return_value = (return_value << 16) | math_random(local);
    return return_value;
}

static n_int people_single_init(simulated_person * entry, n_byte2 *local)
{
    entry->location_x = people_n_byte4_random(local) + BIG_NEGATIVE_INTEGER;
    entry->location_y = people_n_byte4_random(local) + BIG_NEGATIVE_INTEGER;
    entry->location_z = 1;

    entry->date_of_birth = people_n_byte4_random(local);
    entry->occupation = people_n_byte4_random(local);

    entry->genetics[0] = people_n_byte4_random(local);
    entry->genetics[1] = people_n_byte4_random(local);
    entry->genetics[2] = people_n_byte4_random(local);
    entry->genetics[3] = people_n_byte4_random(local);

    return 0;
}

n_int people_init(n_byte2 *local)
{
    n_uint loop = 0;
    
    while (loop < PEOPLE_NUMBER)
    {
        (void)people_single_init(&people[loop], local);
        
        if ((loop & 16383) == 0)
        {
            printf("%ld\n", loop);
        }
        
        loop++;
    }
    
    return 0;
}
