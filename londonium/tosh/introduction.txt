Londinium is Roman London and it also is the name for a simulation that has many parts. I tried to crowd-source what name the simulated Londoner should take and the best name I found through that crowd sourcing was Tosh which means nonsense.

This whole idea of simulating eight and a half million people in London in 1940 was complete nonsense. For a start who were these people who had any interest in simulation and why London in 1940.

It all seemed like Tosh.

Similarly where would you find an understanding of simulating that many people?

How about World War Two?

What about the ethics of simulation?

Thankfully I had spent about thirty years of my life writing agent models and even created communities with other similar simulators. But they didn't exist online and there were no reddit pages that explained any of this stuff. And podcasts, who has the time these days to listen to them.

Every aspect of my prior simulated work had been expunged from the internet as these ideas were clearly marginal and dead.

So thankfully I still maintained the source online and it was now called the ApeSDK.

So when I came to London1940, I had a simulation software development environment that needed to be critically assessed with the view that eight and a half million simulated agents required something new.

That's where it got interesting.

Tom Barbalet, Irvine CA, June 16, 2022.